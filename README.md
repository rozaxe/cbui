# Component Based User Interface

Source code for the series of articles [Conception d'une interface graphique](https://medium.com/@Ax31/conception-dune-interface-graphique-i-sp%C3%A9cifications-6f2f8f209970) (in french).

This is for education and inspiration only, **do not use in production** !

Code under MIT License.
