package cbui;

import cbui.event.KeyCode;
import cbui.event.MouseButton;
import cbui.event.MouseButtonEvent;
import cbui.event.MouseEvent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.image.BufferedImage;
import java.io.InputStream;

public class Printer {

    // Store how many column
    final int column;

    // Store how many line
    final int line;

    // Store the duration two wait between two frames
    final long waiting;

    // Store font used to print
    final private Font font;

    // Store the window display
    final private JFrame frame;
    // Store glyph dimension
    final private Dimension dimension;
    // Store the panel where everything is drawn
    final private Render render;
    // Store shift to apply when printing
    final private int shifting;

    // Store pressed key
    final private boolean[] keyStatus;

    // Store mouse button status
    final private boolean[] mouseButtonStatus;

    final private Point mouse = new Point(0, 0);

    // Buffer used to draw next frame
    private BufferedImage offscreen;

    // Actual drawing object to draw next frame
    private Graphics2D offscreengraphics;

    /**
     * Create an empty window
     */
    public Printer(String title, int width, int height, boolean fullscreen, String fontFilename, float fontSize, int fps) {

        // Maximum character to be drawn
        column = width;
        line = height;

        // 1s divides by FPS requested
        waiting = (long) (1e9 / fps);

        // Initialize font, should not fail
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            InputStream input = classLoader.getResourceAsStream(fontFilename);
            font = Font.createFont(Font.TRUETYPE_FONT, input).deriveFont(fontSize);

        } catch (Exception e) {
            throw new RuntimeException("Load default font failed");
        }

        // Compute font shifting
        Rectangle bounds = getShiftBounds();
        shifting = Math.abs(bounds.y);
        dimension = new Dimension(bounds.width, bounds.height);

        // Create JFrame based on previous computed value
        frame = new JFrame();
        if (fullscreen) {
            frame.setUndecorated(true);
            GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
            gd.setFullScreenWindow(frame);
        }
        frame.setResizable(false);
        frame.setTitle(title);

        // Attach events
        WindowAdapter windowAdapter = new WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent e) {
                //frame.setVisible(false);
                onClose();
            }

            @Override
            public void windowGainedFocus(java.awt.event.WindowEvent e) {
                onFocus();
            }

            @Override
            public void windowLostFocus(java.awt.event.WindowEvent e) {
                onBlur();
            }

        };
        frame.addWindowListener(windowAdapter);
        frame.addWindowFocusListener(windowAdapter);

        // Component used to print
        render = new Render();
        render.addKeyListener(new KeyHandling());
        render.addMouseListener(new MouseHandling());
        render.addMouseMotionListener(new MouseMotionHandling());

        // Set dimension to inside, and repack outside around it
        int realWidth = column * dimension.width;
        int realHeight = line * dimension.height;

        // Double buffer size
        offscreen = new BufferedImage(realWidth, realHeight, BufferedImage.TYPE_INT_ARGB);
        render.onscreen = new BufferedImage(realWidth, realHeight, BufferedImage.TYPE_INT_ARGB);
        offscreengraphics = offscreen.createGraphics();
        render.onscreengraphics = render.onscreen.createGraphics();

        // Pack JFrame leaf
        render.setPreferredSize(new Dimension(realWidth, realHeight));
        frame.setContentPane(render);
        frame.pack();

        // Give keyboard focus (after pack)
        render.setFocusable(true);
        render.requestFocusInWindow();
        KeyCode.init();
        keyStatus = new boolean[KeyCode.max()];

        mouseButtonStatus = new boolean[MouseButton.max()];
    }

    final public boolean isKeyPressed(int code) {
        return keyStatus[code];
    }

    final public boolean isMouseButtonPressed(int button) {
        return mouseButtonStatus[button];
    }

    final public Point getMouse() {
        return mouse;
    }

    /**
     * Start running
     */
    final public void start() {
        onCreate();
        setOpen(true);
        loop();
        setOpen(false);
        onClose();
        frame.dispose();
    }

    public void toCenter() {
        frame.setLocationRelativeTo(null);
    }

    /**
     * Called when JFrame lost focus
     */
    protected void onBlur() {
    }

    /**
     * Called when ready to start
     */
    protected void onCreate() {
    }

    /**
     * Called when JFrame close button is pressed
     */
    protected void onClose() {
    }

    /**
     * Called when JFrame gained focus
     */
    protected void onFocus() {
    }

    /**
     * Called when mouse enter JFrame
     */
    protected void onMouseIn() {
    }

    /**
     * Called when mouse left JFrame
     */
    protected void onMouseOut() {
    }

    /**
     * Called when mouse move in JFrame
     *
     * @param mouseEvent mouse event data
     */
    protected void onMouseMoved(MouseEvent mouseEvent) {
    }

    /**
     * Called when mouse button is pressed in JFrame
     *
     * @param mouseButtonEvent mouse pressed event data
     */
    protected void onMouseButtonPressed(MouseButtonEvent mouseButtonEvent) {
    }

    /**
     * Called when mouse button is released in JFrame
     *
     * @param mouseButtonEvent mouse released event data
     */
    protected void onMouseButtonReleased(MouseButtonEvent mouseButtonEvent) {
    }

    /**
     * Called when key pressed while JFrame has focus
     *
     * @param code key code pressed
     */
    protected void onKeyPressed(int code) {
    }

    /**
     * Called when key released while JFrame has focus
     *
     * @param code key code released
     */
    protected void onKeyReleased(int code) {
    }

    /**
     * Called when character is typed on JFrame
     *
     * @param character Character typed
     */
    protected void onTextEntered(char character) {
    }

    /**
     * Called each frame
     */
    protected void onUpdate(long delta) {
    }

    /**
     * Clear data for reprint
     */
    final protected void clear() {
        Graphics2D g2d = offscreen.createGraphics();
        g2d.clearRect(0, 0, offscreen.getWidth(), offscreen.getHeight());
    }

    /**
     * Print everything since last display
     */
    final protected void display() {

        // Swap offscreen and onscreen
        BufferedImage tmp = offscreen;
        offscreen = render.onscreen;
        render.onscreen = tmp;

        Graphics2D gtmp = offscreengraphics;
        offscreengraphics = render.onscreengraphics;
        render.onscreengraphics = gtmp;

        // Tell render panel to update
        render.repaint();
    }

    /**
     * Loop to update app
     */
    protected void loop() {

        long previous = System.nanoTime();

        while (isOpen()) {

            // Wait till next frame
            long now = System.nanoTime();
            long delta = now - previous;
            while (delta < waiting) {
                now = System.nanoTime();
                delta = now - previous;
            }
            previous = now;

            // Clear, update, display
            clear();
            onUpdate(delta);
            display();
        }
    }

    /**
     * Get JFrame's visibility
     *
     * @return JFrame's visibility status
     */
    final protected boolean isOpen() {
        return frame.isVisible();
    }

    final protected void setOpen(boolean status) {
        frame.setVisible(status);
    }

    /**
     * Print the given character at the given coordinate
     */
    final protected void print(int x, int y, String string, Color fill, Color background) {
        Graphics2D g2d = offscreen.createGraphics();
        g2d.setFont(font);
        g2d.setColor(background);
        g2d.fillRect(x * dimension.width, y * dimension.height, dimension.width, dimension.height);
        g2d.setColor(fill);
        g2d.drawString(string, x * dimension.width, y * dimension.height + shifting);
    }

    // Find shifting applied to text printing
    private Rectangle getShiftBounds() {

        GlyphVector gv;
        Rectangle pixelBounds;

        // Virtual renderer
        BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = image.createGraphics();
        FontRenderContext frc = g2d.getFontRenderContext();
        gv = font.createGlyphVector(frc, "\u2588"); // Full square code
        pixelBounds = gv.getPixelBounds(frc, 0, 0);

        // Data is stored directly in variable
        return pixelBounds;
    }

    private class Render extends JPanel {

        BufferedImage onscreen;// = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        Graphics2D onscreengraphics;

        public Render() {
            // Set clearing color
            setBackground(Color.BLACK);

            // Set text filling color
            setForeground(Color.WHITE);
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.drawImage(onscreen, 0, 0, null);
        }
    }

    private class KeyHandling implements java.awt.event.KeyListener {

        @Override
        public void keyPressed(KeyEvent e) {
            int key = e.getKeyCode();
            if (KeyCode.isKnown(key)) {
                keyStatus[key] = true;
                onKeyPressed(key);
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
            int key = e.getKeyCode();
            if (KeyCode.isKnown(key)) {
                keyStatus[key] = false;
                onKeyReleased(key);
            }
        }

        @Override
        public void keyTyped(KeyEvent e) {
            onTextEntered(e.getKeyChar());
        }
    }

    private class MouseHandling implements java.awt.event.MouseListener {

        @Override
        public void mouseClicked(java.awt.event.MouseEvent e) {
        }

        @Override
        public void mousePressed(java.awt.event.MouseEvent e) {
            if (MouseButton.isKnow(e.getButton())) {
                int cellX = e.getX() / dimension.width;
                int cellY = e.getY() / dimension.height;

                mouse.x = cellX;
                mouse.y = cellY;
                mouseButtonStatus[e.getButton()] = true;

                onMouseButtonPressed(new MouseButtonEvent(cellX, cellY, e.getButton()));
            }
        }

        @Override
        public void mouseReleased(java.awt.event.MouseEvent e) {
            if (MouseButton.isKnow(e.getButton())) {
                int cellX = e.getX() / dimension.width;
                int cellY = e.getY() / dimension.height;

                mouse.x = cellX;
                mouse.y = cellY;
                mouseButtonStatus[e.getButton()] = false;

                onMouseButtonReleased(new MouseButtonEvent(cellX, cellY, e.getButton()));
            }
        }

        @Override
        public void mouseEntered(java.awt.event.MouseEvent e) {
            onMouseIn();
        }

        @Override
        public void mouseExited(java.awt.event.MouseEvent e) {
            onMouseOut();
        }
    }

    private class MouseMotionHandling implements java.awt.event.MouseMotionListener {

        private void common(java.awt.event.MouseEvent e) {
            int cellX = e.getX() / dimension.width;
            int cellY = e.getY() / dimension.height;

            mouse.x = cellX;
            mouse.y = cellY;

            onMouseMoved(new MouseEvent(cellX, cellY));
        }

        @Override
        public void mouseDragged(java.awt.event.MouseEvent e) {
            common(e);
        }

        @Override
        public void mouseMoved(java.awt.event.MouseEvent e) {
            common(e);
        }
    }

}
