package cbui;

import cbui.event.listener.*;

import java.awt.*;
import java.util.Vector;

public abstract class Component {

    // Store leaf coordinate and dimension
    int x;
    int y;
    int width;
    int height;
    int absoluteWidth;
    int absoluteHeight;
    boolean visible;
    boolean hovered = false;

    int preferredWidth;
    int preferredHeight;
    private Board board;

    private KeyListener laterKeyListener;
    private MouseListener laterMouseListener;
    private MouseButtonListener laterMouseButtonListener;
    private TextListener laterTextListener;
    private WindowListener laterWindowListener;

    public Component() {
        visible = true;
        preferredWidth = 0;
        preferredHeight = 0;
    }

    public Component(int width, int height) {
        visible = true;
        preferredWidth = width;
        preferredHeight = height;
    }

    public void setPreferredWidth(int preferredWidth) {
        this.preferredWidth = preferredWidth;
    }

    public void setPreferredHeight(int preferredHeight) {
        this.preferredHeight = preferredHeight;
    }

    public final void addKeyListener(KeyListener listener) {
        laterKeyListener = listener;
    }

    public final void addMouseListener(MouseListener listener) {
        laterMouseListener = listener;
    }

    public final void addMouseButtonListener(MouseButtonListener listener) {
        laterMouseButtonListener = listener;
    }

    public final void addWindowListener(WindowListener listener) {
        laterWindowListener = listener;
    }

    public final void addTextListener(TextListener listener) {
        laterTextListener = listener;
    }

    public final int getX() {
        return x;
    }

    public final int getY() {
        return y;
    }

    public final int getWidth() {
        return width;
    }

    public final int getHeight() {
        return height;
    }

    public final boolean isHovered() {
        return hovered;
    }

    // Called each time leaf needs to be redraw
    // delta in nanoseconds
    public void onUpdate(long delta) {
    }

    public void onRedraw() {
    }

    public void onResize() {
    }

    public final void print(int x, int y, String str) {
        if ((0 <= x && x < this.width) &&
                (0 <= y && y < this.height)) {
            board.print(this.x + x, this.y + y, str, Color.WHITE, Color.BLACK);
        }
    }

    // Print if coordinate in leaf
    public final void print(int x, int y, char character) {
        print(x, y, character, Color.WHITE, Color.BLACK);
    }

    public final void print(int x, int y, char character, Color fill) {
        print(x, y, character, fill, Color.BLACK);
    }

    public final void print(int x, int y, char character, Color fill, Color background) {
        if ((0 <= x && x < this.width) &&
                (0 <= y && y < this.height)) {
            board.print(this.x + x, this.y + y, Character.toString(character), fill, background);
        }
    }

    public final void setVisible(boolean status) {
        visible = status;
        setDirty();
    }

    public final void setDirty() {
        if (board != null) {
            board.dirty = true;
        }
    }

    void attachListener() {
        if (laterMouseListener != null) {
            board.registerMouseEvent(this, laterMouseListener);
        }
        if (laterMouseButtonListener != null) {
            board.registerMouseButtonEvent(this, laterMouseButtonListener);
        }
        if (laterKeyListener != null) {
            board.registerKeyEvent(this, laterKeyListener);
        }
        if (laterTextListener != null) {
            board.registerTextEvent(this, laterTextListener);
        }
        if (laterWindowListener != null) {
            board.registerWindowEvent(this, laterWindowListener);
        }
    }

    final boolean containsAbsolute(int x, int y) {
        return (this.x <= x && x < absoluteWidth) &&
                (this.y <= y && y < absoluteHeight);
    }

    void pack(Vector<Component> componentsVisible) {
        if (visible) {
            componentsVisible.add(this);
            attachListener();
            onResize();
        }
    }

    void propagate(Board board) {
        this.board = board;
        attachListener();
    }

}
