package cbui;

import java.util.Vector;

public abstract class Composite extends Component {

    protected Vector<ComponentBuilder> components = new Vector<>();

    public Composite() {
    }

    public Composite(int width, int height) {
        super(width, height);
    }

    // Tell each child who is board
    final void propagate(Board board) {
        super.propagate(board);

        // Propagate to each child
        for (ComponentBuilder c : components) {
            c.build.propagate(board);
        }
    }

    protected void attachListener() {
        super.attachListener();
    }

    protected void pack(Vector<Component> componentsVisible) {
        super.pack(componentsVisible);
    }

}
