package cbui;

import cbui.event.MouseButtonEvent;
import cbui.event.MouseEvent;
import cbui.event.listener.*;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Vector;

public class Board extends Printer {

    private final Queue<Event> events = new ArrayDeque<>();

    private final Vector<Component> componentsVisible = new Vector<>();

    private final Vector<MousePaired> mouseListeners = new Vector<>();

    private final Vector<MouseButtonPaired> mouseButtonListeners = new Vector<>();

    private final Vector<KeyListener> keyListeners = new Vector<>();

    private final Vector<TextListener> textListeners = new Vector<>();

    private final Vector<WindowListener> windowListeners = new Vector<>();

    boolean dirty = false;

    // Store the root leaf of the board
    private Component component;

    public Board(String title, int width, int height, boolean fullscreen, String fontFilename, float fontSize, int fps) {
        super(title, width, height, fullscreen, fontFilename, fontSize, fps);
    }

    //region Synchronize event

    @Override
    final protected void onBlur() {
        Event event = new Event(EventType.onBlur);
        synchronized (events) {
            events.add(event);
        }
    }

    @Override
    final protected void onCreate() {
        toCenter();
    }

    @Override
    final protected void onClose() {
        Event event = new Event(EventType.onClose);
        synchronized (events) {
            events.add(event);
        }
    }

    @Override
    final protected void onFocus() {
        Event event = new Event(EventType.onFocus);
        synchronized (events) {
            events.add(event);
        }
    }

    @Override
    final protected void onMouseIn() {
        // Not used
    }

    @Override
    final protected void onMouseOut() {
        Event event = new Event(EventType.onMouseOut);
        synchronized (events) {
            events.add(event);
        }
    }

    @Override
    final protected void onMouseMoved(MouseEvent mouseEvent) {
        Event event = new Event(EventType.onMouseMoved);
        event.mouseEvent = mouseEvent;
        synchronized (events) {
            events.add(event);
        }
    }

    @Override
    final protected void onMouseButtonPressed(MouseButtonEvent mouseButtonEvent) {
        Event event = new Event(EventType.onMousePressed);
        event.mouseButtonEvent = mouseButtonEvent;
        synchronized (events) {
            events.add(event);
        }
    }

    @Override
    final protected void onMouseButtonReleased(MouseButtonEvent mouseButtonEvent) {
        Event event = new Event(EventType.onMouseReleased);
        event.mouseButtonEvent = mouseButtonEvent;
        synchronized (events) {
            events.add(event);
        }
    }

    @Override
    final protected void onKeyPressed(int code) {
        Event event = new Event(EventType.onKeyPressed);
        event.keyCode = code;
        synchronized (events) {
            events.add(event);
        }
    }

    @Override
    final protected void onKeyReleased(int code) {
        Event event = new Event(EventType.onKeyReleased);
        event.keyCode = code;
        synchronized (events) {
            events.add(event);
        }
    }

    @Override
    final protected void onTextEntered(char character) {
        Event event = new Event(EventType.onTextEntered);
        event.character = character;
        synchronized (events) {
            events.add(event);
        }
    }

    @Override
    final protected void onUpdate(long delta) {
        // Not used
    }

    //endregion

    //region Registers

    final void registerMouseEvent(Component component, MouseListener listener) {
        MousePaired mousePaired = new MousePaired(component, listener);
        mouseListeners.add(mousePaired);
    }

    final void registerMouseButtonEvent(Component component, MouseButtonListener listener) {
        MouseButtonPaired mouseButtonPaired = new MouseButtonPaired(component, listener);
        mouseButtonListeners.add(mouseButtonPaired);
    }

    final void registerKeyEvent(Component component, KeyListener listener) {
        keyListeners.add(listener);
    }

    final void registerTextEvent(Component component, TextListener listener) {
        textListeners.add(listener);
    }

    final void registerWindowEvent(Component component, WindowListener listener) {
        windowListeners.add(listener);
    }

    //endregion

    //region Dispatch

    private void dispatchMouseOut() {
        for (MousePaired pair : mouseListeners) {
            if (pair.component.hovered) {
                pair.component.hovered = false;
                pair.listener.onMouseOut();
            }
        }
    }

    private void dispatchMouseMoved(MouseEvent mouseEvent) {
        for (MousePaired pair : mouseListeners) {

            if (pair.component.containsAbsolute(mouseEvent.x, mouseEvent.y)) {

                // In
                if (!pair.component.hovered) {
                    pair.component.hovered = true;
                    pair.listener.onMouseIn();
                }

                // Move
                pair.listener.onMouseMoved(new MouseEvent(
                        mouseEvent.x - pair.component.x,
                        mouseEvent.y - pair.component.y
                ));

            } else {

                // Out
                if (pair.component.hovered) {
                    pair.component.hovered = false;
                    pair.listener.onMouseOut();
                }
            }
        }
    }

    private void dispatchMousePressed(MouseButtonEvent mouseButtonEvent) {
        for (MouseButtonPaired pair : mouseButtonListeners) {
            if (pair.component.containsAbsolute(mouseButtonEvent.x, mouseButtonEvent.y)) {
                pair.listener.onMouseButtonPressed(new MouseButtonEvent(
                        mouseButtonEvent.x - pair.component.x,
                        mouseButtonEvent.y - pair.component.y,
                        mouseButtonEvent.button));
            }
        }
    }

    private void dispatchMouseReleased(MouseButtonEvent mouseButtonEvent) {
        for (MouseButtonPaired pair : mouseButtonListeners) {
            if (pair.component.containsAbsolute(mouseButtonEvent.x, mouseButtonEvent.y)) {
                pair.listener.onMouseButtonReleased(new MouseButtonEvent(
                        mouseButtonEvent.x - pair.component.x,
                        mouseButtonEvent.y - pair.component.y,
                        mouseButtonEvent.button));
            }
        }
    }

    private void dispatchKeyPressed(int code) {
        for (KeyListener listener : keyListeners) {
            listener.onKeyPressed(code);
        }
    }

    private void dispatchKeyReleased(int code) {
        for (KeyListener listener : keyListeners) {
            listener.onKeyReleased(code);
        }
    }

    private void dispatchTextEntered(char character) {
        for (TextListener listener : textListeners) {
            listener.onTextEntered(character);
        }
    }

    private void dispatchBlurEvent() {
        for (WindowListener listener : windowListeners) {
            listener.onBlur();
        }
    }

    private void dispatchCloseEvent() {
        for (WindowListener listener : windowListeners) {
            listener.onClose();
        }
    }

    private void dispatchFocusEvent() {
        for (WindowListener listener : windowListeners) {
            listener.onFocus();
        }
    }

    private void unregisterAll() {
        mouseListeners.clear();
        mouseButtonListeners.clear();
        keyListeners.clear();
        textListeners.clear();
        windowListeners.clear();
    }

    //endregion

    @Override
    protected void loop() {

        long previous = System.nanoTime();

        repack();

        while (isOpen()) {

            // Wait till next frame
            long now = System.nanoTime();
            long delta = now - previous;
            previous = now;

            // Clear, update, display
            clear();
            dispatchRedraw();
            display();

            /* Active waiting is not okay !
            while (delta < waiting) {
                now = System.nanoTime();
                delta = now - previous;
            }
            previous = now;
            //*/

            checkDirty();
            dispatchEvents();

            checkDirty();
            dispatchUpdate(delta);

            //* Passive waiting is better, even if it is not optimal in Java...
            try {
                Thread.sleep(33);
            } catch (InterruptedException ignored) {
            }
            //*/
        }
    }

    /**
     * Set inside leaf
     *
     * @param component the leaf to print
     */
    public void set(Component component) {
        this.component = component;
        this.component.propagate(this);
    }

    private void checkDirty() {
        if (dirty) {
            dirty = false;
            repack();
        }
    }

    // Repack components size
    private void repack() {
        // Empty components to redraw
        componentsVisible.clear();

        // Give all space to the root leaf
        component.x = 0;
        component.y = 0;
        component.width = column;
        component.height = line;
        component.absoluteWidth = column;
        component.absoluteHeight = line;

        // Remove all listeners
        unregisterAll();

        // Pack everything inside
        component.pack(componentsVisible);
    }

    private void dispatchRedraw() {
        for (Component component : componentsVisible) {
            component.onRedraw();
        }
    }

    private void dispatchEvents() {
        synchronized (events) {
            Event event = events.poll();
            while (event != null) {
                switch (event.type) {

                    case onMouseMoved:
                        dispatchMouseMoved(event.mouseEvent);
                        break;

                    case onMousePressed:
                        dispatchMousePressed(event.mouseButtonEvent);
                        break;

                    case onMouseReleased:
                        dispatchMouseReleased(event.mouseButtonEvent);
                        break;

                    case onKeyPressed:
                        dispatchKeyPressed(event.keyCode);
                        break;

                    case onKeyReleased:
                        dispatchKeyReleased(event.keyCode);
                        break;

                    case onTextEntered:
                        dispatchTextEntered(event.character);
                        break;

                    case onMouseOut:
                        dispatchMouseOut();
                        break;

                    case onBlur:
                        dispatchBlurEvent();
                        break;

                    case onFocus:
                        dispatchFocusEvent();
                        break;

                    case onClose:
                        dispatchCloseEvent();
                        break;

                }
                event = events.poll();
            }
        }
    }

    private void dispatchUpdate(long delta) {
        for (Component component : componentsVisible) {
            component.onUpdate(delta);
        }
    }

    private enum EventType {
        onMouseOut,
        onMouseMoved,
        onMousePressed,
        onMouseReleased,
        onKeyPressed,
        onKeyReleased,
        onTextEntered,
        onBlur,
        onFocus,
        onClose,
    }

    private class Event {

        EventType type;
        MouseEvent mouseEvent;
        MouseButtonEvent mouseButtonEvent;
        int keyCode;
        char character;

        Event(EventType type) {
            this.type = type;
        }
    }

    private class MousePaired {

        Component component;
        MouseListener listener;

        MousePaired(Component component, MouseListener listener) {
            this.component = component;
            this.listener = listener;
        }

    }

    private class MouseButtonPaired {

        Component component;
        MouseButtonListener listener;

        MouseButtonPaired(Component component, MouseButtonListener listener) {
            this.component = component;
            this.listener = listener;
        }
    }
}
