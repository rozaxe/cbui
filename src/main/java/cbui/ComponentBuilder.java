package cbui;

import java.util.Vector;

public final class ComponentBuilder {

    Component build;

    public ComponentBuilder(Component component) {
        build = component;
    }

    public int getX() {
        return build.x;
    }

    public void setX(int x) {
        build.x = x;
    }

    public int getY() {
        return build.y;
    }

    public void setY(int y) {
        build.y = y;
    }

    public int getWidth() {
        return build.width;
    }

    public void setWidth(int width) {
        build.width = width;
        build.absoluteWidth = build.x + build.width;
    }

    public int getHeight() {
        return build.height;
    }

    public void setHeight(int height) {
        build.height = height;
        build.absoluteHeight = build.y + build.height;
    }

    public int getPreferredWidth() {
        return build.preferredWidth;
    }

    public int getPreferredHeight() {
        return build.preferredHeight;
    }

    public boolean isVisible() {
        return build.visible;
    }

    public void pack(Vector<Component> componentsVisible) {
        build.pack(componentsVisible);
    }

}
