package cbui.event;

public class MouseEvent {

    public final int x;
    public final int y;

    public MouseEvent(int x, int y) {
        this.x = x;
        this.y = y;
    }

}
