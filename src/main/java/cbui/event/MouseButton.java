package cbui.event;

import java.awt.event.MouseEvent;

public class MouseButton {

    public static final int LEFT = MouseEvent.BUTTON1;
    public static final int MIDDLE = MouseEvent.BUTTON2;
    public static final int RIGHT = MouseEvent.BUTTON3;

    private MouseButton() {
    }

    public static boolean isKnow(int code) {
        return (code == LEFT) || (code == MIDDLE) || (code == RIGHT);
    }

    public static int max() {
        return RIGHT + 1;
    }

}
