package cbui.event;

import java.awt.event.KeyEvent;

public class KeyCode {

    public static final int LEFT = KeyEvent.VK_LEFT;
    public static final int UP = KeyEvent.VK_UP;
    public static final int RIGHT = KeyEvent.VK_RIGHT;
    public static final int DOWN = KeyEvent.VK_DOWN;
    public static final int A = KeyEvent.VK_A;
    public static final int B = KeyEvent.VK_B;
    public static final int C = KeyEvent.VK_C;
    public static final int D = KeyEvent.VK_D;
    public static final int E = KeyEvent.VK_E;
    public static final int F = KeyEvent.VK_F;
    public static final int G = KeyEvent.VK_G;
    public static final int H = KeyEvent.VK_H;
    public static final int I = KeyEvent.VK_I;
    public static final int J = KeyEvent.VK_J;
    public static final int K = KeyEvent.VK_K;
    public static final int L = KeyEvent.VK_L;
    public static final int M = KeyEvent.VK_M;
    public static final int N = KeyEvent.VK_N;
    public static final int O = KeyEvent.VK_O;
    public static final int P = KeyEvent.VK_P;
    public static final int Q = KeyEvent.VK_Q;
    public static final int R = KeyEvent.VK_R;
    public static final int S = KeyEvent.VK_S;
    public static final int T = KeyEvent.VK_T;
    public static final int U = KeyEvent.VK_U;
    public static final int V = KeyEvent.VK_V;
    public static final int W = KeyEvent.VK_W;
    public static final int X = KeyEvent.VK_X;
    public static final int Y = KeyEvent.VK_Y;
    public static final int Z = KeyEvent.VK_Z;
    public static final int NUM0 = KeyEvent.VK_0;
    public static final int NUM1 = KeyEvent.VK_1;
    public static final int NUM2 = KeyEvent.VK_2;
    public static final int NUM3 = KeyEvent.VK_3;
    public static final int NUM4 = KeyEvent.VK_4;
    public static final int NUM5 = KeyEvent.VK_5;
    public static final int NUM6 = KeyEvent.VK_6;
    public static final int NUM7 = KeyEvent.VK_7;
    public static final int NUM8 = KeyEvent.VK_8;
    public static final int NUM9 = KeyEvent.VK_9;
    public static final int F1 = KeyEvent.VK_F1;
    public static final int F2 = KeyEvent.VK_F2;
    public static final int F3 = KeyEvent.VK_F3;
    public static final int F4 = KeyEvent.VK_F4;
    public static final int F5 = KeyEvent.VK_F5;
    public static final int F6 = KeyEvent.VK_F6;
    public static final int F7 = KeyEvent.VK_F7;
    public static final int F8 = KeyEvent.VK_F8;
    public static final int F9 = KeyEvent.VK_F9;
    public static final int CONTROL = KeyEvent.VK_CONTROL;
    public static final int SHIFT = KeyEvent.VK_SHIFT;
    public static final int ALT = KeyEvent.VK_ALT;
    public static final int SYSTEM = KeyEvent.VK_META; // MAX 157
    public static final int ESCAPE = KeyEvent.VK_ESCAPE;
    public static final int BACKSPACE = KeyEvent.VK_BACK_SPACE;
    public static final int DELETE = KeyEvent.VK_DELETE;
    public static final int TAB = KeyEvent.VK_TAB;
    public static final int ENTER = KeyEvent.VK_ENTER;
    public static final int SPACE = KeyEvent.VK_SPACE;
    private static final boolean[] keysKnown = new boolean[160];

    private KeyCode() {
    }

    public static void init() {
        keysKnown[LEFT] = true;
        keysKnown[UP] = true;
        keysKnown[RIGHT] = true;
        keysKnown[DOWN] = true;
        keysKnown[A] = true;
        keysKnown[B] = true;
        keysKnown[C] = true;
        keysKnown[D] = true;
        keysKnown[E] = true;
        keysKnown[F] = true;
        keysKnown[G] = true;
        keysKnown[H] = true;
        keysKnown[I] = true;
        keysKnown[J] = true;
        keysKnown[K] = true;
        keysKnown[L] = true;
        keysKnown[M] = true;
        keysKnown[N] = true;
        keysKnown[O] = true;
        keysKnown[P] = true;
        keysKnown[Q] = true;
        keysKnown[R] = true;
        keysKnown[S] = true;
        keysKnown[T] = true;
        keysKnown[U] = true;
        keysKnown[V] = true;
        keysKnown[W] = true;
        keysKnown[X] = true;
        keysKnown[Y] = true;
        keysKnown[Z] = true;
        keysKnown[NUM0] = true;
        keysKnown[NUM1] = true;
        keysKnown[NUM2] = true;
        keysKnown[NUM3] = true;
        keysKnown[NUM4] = true;
        keysKnown[NUM5] = true;
        keysKnown[NUM6] = true;
        keysKnown[NUM7] = true;
        keysKnown[NUM8] = true;
        keysKnown[NUM9] = true;
        keysKnown[F1] = true;
        keysKnown[F2] = true;
        keysKnown[F3] = true;
        keysKnown[F4] = true;
        keysKnown[F5] = true;
        keysKnown[F6] = true;
        keysKnown[F7] = true;
        keysKnown[F8] = true;
        keysKnown[F9] = true;
        keysKnown[CONTROL] = true;
        keysKnown[SHIFT] = true;
        keysKnown[ALT] = true;
        keysKnown[SYSTEM] = true;
        keysKnown[ESCAPE] = true;
        keysKnown[BACKSPACE] = true;
        keysKnown[DELETE] = true;
        keysKnown[TAB] = true;
        keysKnown[ENTER] = true;
        keysKnown[SPACE] = true;
    }

    public static boolean isKnown(int code) {
        try {
            return keysKnown[code];
        } catch (ArrayIndexOutOfBoundsException e) {
            return false;
        }
    }

    public static int max() {
        return keysKnown.length;
    }

}
