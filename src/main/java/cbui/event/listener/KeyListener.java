package cbui.event.listener;

public interface KeyListener {

    void onKeyPressed(int code);

    void onKeyReleased(int code);

}
