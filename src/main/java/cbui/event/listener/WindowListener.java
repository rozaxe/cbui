package cbui.event.listener;

public interface WindowListener {

    void onBlur();

    void onClose();

    void onFocus();

}
