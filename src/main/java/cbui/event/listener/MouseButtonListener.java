package cbui.event.listener;

import cbui.event.MouseButtonEvent;

public interface MouseButtonListener {

    void onMouseButtonPressed(MouseButtonEvent event);

    void onMouseButtonReleased(MouseButtonEvent event);

}
