package cbui.event.listener;

public interface TextListener {

    void onTextEntered(char character);

}
