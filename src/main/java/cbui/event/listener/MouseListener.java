package cbui.event.listener;

import cbui.event.MouseEvent;

public interface MouseListener {

    void onMouseIn();

    void onMouseOut();

    void onMouseMoved(MouseEvent event);

}
