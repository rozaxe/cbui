package cbui.composite;

import cbui.Component;

public class Scene {

    Component component;

    State state = State.SLEEPING;

    public Scene() {
    }

    public Scene(Component component) {
        this.component = component;
    }

    public void set(Component component) {
        this.component = component;
    }

    public void onCreate() {
    }

    public void onDispose() {
    }

}
