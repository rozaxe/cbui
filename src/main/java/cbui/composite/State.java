package cbui.composite;

public enum State {
    SLEEPING,
    RUNNING,
    PAUSED
}
