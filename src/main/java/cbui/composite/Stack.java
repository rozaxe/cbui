package cbui.composite;

import cbui.Component;
import cbui.ComponentBuilder;
import cbui.Composite;

import java.awt.*;
import java.util.Vector;

public class Stack extends Composite {

    private Orientation orientation;

    public Stack(Orientation orientation) {
        this.orientation = orientation;
    }

    public Stack(Orientation orientation, int width, int height) {
        super(width, height);
        this.orientation = orientation;
    }

    public final void add(Component component) {
        components.add(new ComponentBuilder(component));
    }

    final public void addSeparator() {
        if (orientation == Orientation.Horizontal) {
            add(new VerticalSeparator());
        } else {
            add(new HorizontalSeparator());
        }
    }

    @Override
    protected final void pack(Vector<Component> componentsVisible) {
        if (orientation == Orientation.Horizontal) {
            horizontal(componentsVisible);

        } else {
            // Vertical
            vertical(componentsVisible);
        }

        attachListener();
        onResize();
    }

    private void vertical(Vector<Component> componentsVisible) {
        Vector<ComponentBuilder> fluids = new Vector<>();
        Vector<ComponentBuilder> visibles = new Vector<>();

        // Only height is respected
        int many_fixed_height = 0; // Store sum of preferred width leaf
        int shift = getY();
        int remaining = getHeight();

        // Get full width required
        for (ComponentBuilder c : components) {
            if (c.isVisible()) {
                visibles.add(c);
                if (c.getPreferredHeight() == 0) {
                    fluids.add(c);
                } else {
                    many_fixed_height += c.getPreferredHeight();
                }
            }
        }

        // Compute width per fluid leaf
        int one_fluid_height = 0;
        if (!fluids.isEmpty()) {
            one_fluid_height = (getHeight() - many_fixed_height - (fluids.size() - 1)) / fluids.size();
        }

        // Attribute effective height
        for (int i = 0; i < visibles.size() - 1; ++i) {
            ComponentBuilder c = visibles.get(i);

            if (c.getPreferredHeight() == 0) {
                apply(visibles.get(i), getX(), shift, getWidth(), one_fluid_height);

            } else {
                apply(visibles.get(i), getX(), shift, getWidth(), c.getPreferredHeight());

            }

            // Shift for next leaf, and create border
            shift += c.getHeight();

            // Pack everything inside leaf
            c.pack(componentsVisible);

            remaining -= c.getHeight();
        }

        // Last leaf get remaining space
        ComponentBuilder last = visibles.lastElement();
        apply(last, getX(), shift, getWidth(), remaining);
        last.pack(componentsVisible);
    }

    private void horizontal(Vector<Component> componentsVisible) {
        Vector<ComponentBuilder> fluids = new Vector<>();
        Vector<ComponentBuilder> visibles = new Vector<>();

        // Only height is respected
        int many_fixed_width = 0; // Store sum of preferred width leaf
        int shift = getX();
        int remaining = getWidth();

        // Get full width required
        for (ComponentBuilder c : components) {
            if (c.isVisible()) {
                visibles.add(c);
                if (c.getPreferredWidth() == 0) {
                    fluids.add(c);
                } else {
                    many_fixed_width += c.getPreferredWidth();
                }
            }
        }

        // Compute width per fluid leaf
        int one_fluid_width = 0;
        if (!fluids.isEmpty()) {
            one_fluid_width = (getWidth() - many_fixed_width - (fluids.size() - 1)) / fluids.size();
        }

        // Attribute effective height
        for (int i = 0; i < visibles.size() - 1; ++i) {
            ComponentBuilder c = visibles.get(i);

            if (!c.isVisible()) {
                continue;
            }

            if (c.getPreferredWidth() == 0) {
                apply(visibles.get(i), shift, getY(), one_fluid_width, getHeight());

            } else {
                apply(visibles.get(i), shift, getY(), c.getPreferredWidth(), getHeight());

            }

            // Shift for next leaf, and create border
            shift += c.getWidth();

            // Pack everything inside leaf
            c.pack(componentsVisible);

            remaining -= c.getWidth();
        }

        // Last leaf get remaining space
        ComponentBuilder last = visibles.lastElement();
        apply(last, shift, getY(), remaining, getHeight());
        last.pack(componentsVisible);
    }

    private void apply(ComponentBuilder c, int x, int y, int width, int height) {
        c.setX(x);
        c.setY(y);
        c.setWidth(width);
        c.setHeight(height);
    }

    private class VerticalSeparator extends Component {

        public VerticalSeparator() {
            super(1, 0);
        }

        @Override
        public void onUpdate(long delta) {
        }

        @Override
        public void onRedraw() {
            Color c = new Color(0.3f, 0.3f, 0.3f);
            for (int i = 0; i < getHeight(); ++i) {
                print(0, i, '|', new Color(0.2f, 0.2f, 0.2f), c);
            }
        }
    }

    private class HorizontalSeparator extends Component {

        public HorizontalSeparator() {
            super(0, 1);
        }

        @Override
        public void onUpdate(long delta) {
        }

        @Override
        public void onRedraw() {
            Color c = new Color(0.3f, 0.3f, 0.3f);
            for (int i = 0; i < getWidth(); ++i) {
                print(i, 0, '-', new Color(0.2f, 0.2f, 0.2f), c);
            }
        }
    }
}
