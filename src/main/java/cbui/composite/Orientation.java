package cbui.composite;

public enum Orientation {
    Horizontal,
    Vertical
}
