package cbui.composite;

import cbui.Component;

public class Inline extends Stack {

    public Inline() {
        super(Orientation.Horizontal, 0, 1);
    }

    public Inline pipe(Component component) {
        add(component);
        return this;
    }

}
