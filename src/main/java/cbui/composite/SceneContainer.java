package cbui.composite;

import cbui.Component;
import cbui.ComponentBuilder;
import cbui.Composite;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class SceneContainer extends Composite {

    private String running;

    private String next;

    private Map<String, Scene> states = new HashMap<>();

    /**
     * Define a new scene
     * By default, last leaf added will start
     *
     * @param name  a string key representing the scene
     * @param scene the scene attached to the string
     */
    public final void add(String name, Scene scene) {
        components.add(new ComponentBuilder(scene.component));

        states.put(name, scene);
        running = name;
        next = name;
    }

    /**
     * Set scene to start
     *
     * @param newScene the scene to start
     */
    public final void start(String newScene) {
        next = newScene;
        setDirty();
    }

    @Override
    protected final void pack(Vector<Component> componentsVisible) {
        //super.pack(componentsVisible);
        ComponentBuilder builder = new ComponentBuilder(states.get(next).component);

        // Give all space to active leaf
        builder.setX(getX());
        builder.setY(getY());
        builder.setWidth(getWidth());
        builder.setHeight(getHeight());

        // Pack inside scene
        builder.pack(componentsVisible);

        // Handle scene state sender
        if (running.equals(next)) {
            Scene current = states.get(running);

            if (current.state == State.SLEEPING) {
                // Start scene only if previously sleeping
                current.onCreate();
                current.state = State.RUNNING;
            }

        } else {
            // Different scene
            Scene old = states.get(running);
            Scene current = states.get(next);

            if (old.state == State.RUNNING) {
                old.onDispose();
                old.state = State.SLEEPING;
            }
            current.onCreate();
            current.state = State.RUNNING;

            running = next;
        }
    }
}
