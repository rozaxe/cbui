package cbui.component;

import cbui.Component;
import cbui.event.KeyCode;
import cbui.event.MouseButtonEvent;
import cbui.event.listener.KeyListener;
import cbui.event.listener.MouseButtonListener;

import java.awt.*;
import java.util.function.Consumer;

public class Select extends Component implements KeyListener, MouseButtonListener {

    private Consumer<Integer> onSelect;

    private String[] options;

    private int selected = 0;
    private int start = 0;

    public Select(String[] options, int height) {
        super(0, height);
        setOptions(options);

        addKeyListener(this);
        addMouseButtonListener(this);
    }

    public final void setOptions(String[] options) {
        if (options.length == 0) {
            throw new RuntimeException("Options should not be empty");
        }
        this.options = options;
        selected = 0;
        start = 0;
    }

    public final int getSelected() {
        return start + selected;
    }

    private void setSelected(int i) {
        selected = i;
        if (onSelect != null) {
            onSelect.accept(getSelected());
        }
    }

    public final String getSelectedAsString() {
        return options[start + selected];
    }

    @Override
    public final void onUpdate(long delta) {
    }

    @Override
    public final void onRedraw() {
        // Print options
        int i = 0;
        do {

            print(1, i, options[start + i]);
            ++i;

        } while (i < getHeight() && i < options.length);

        // Print selector
        print(0, selected, '>');

        // Print arrows
        print(getWidth() - 1, 0, '▲', Color.WHITE, Color.GRAY);
        print(getWidth() - 1, 1, '▼', Color.WHITE, Color.GRAY);
    }

    @Override
    public void onKeyPressed(int code) {
        if (code == KeyCode.UP) {
            goUp();
        } else if (code == KeyCode.DOWN) {
            goDown();
        }
    }

    @Override
    public void onKeyReleased(int code) {
    }

    @Override
    public void onMouseButtonPressed(MouseButtonEvent event) {
        if (event.x == getWidth() - 1) {
            if (event.y == 0) {
                goUp();
            } else if (event.y == 1) {
                goDown();
            }
        } else {
            if (event.y < options.length) {
                setSelected(event.y);
            }
        }
    }

    @Override
    public void onMouseButtonReleased(MouseButtonEvent event) {
    }

    private void goUp() {
        if (selected == 0) {
            // Already on top
            if (start > 0) {
                // Not top of list
                --start;
                setSelected(0);
            }

        } else {
            // Middle of list
            setSelected(selected - 1);
        }
    }

    private void goDown() {
        if (selected == getHeight() - 1) {
            // Already on bottom
            if (start + selected + 1 < options.length) {
                // Not bottom of list
                ++start;
                setSelected(getHeight() - 1);
            }
        } else {
            // Middle of list
            int min = Math.min(options.length - 1, selected + 1);
            setSelected(min);

        }
    }

    public void setOnSelect(Consumer<Integer> onSelect) {
        this.onSelect = onSelect;
    }
}
