package cbui.component;

import cbui.Component;

public class FrameCount extends Component {

    private int lastFPS = 0;
    private int currentFPS = 0;
    private long delta = 0;

    public FrameCount() {
        super(4, 1);
    }

    @Override
    public void onUpdate(long delta) {
        ++currentFPS;

        this.delta += delta;
        if (this.delta >= 1e9) {
            lastFPS = currentFPS;
            this.delta = 0;
            currentFPS = 0;
        }
    }

    @Override
    public void onRedraw() {
        print(0, 0, String.valueOf(lastFPS));
    }

}
