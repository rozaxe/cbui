package cbui.component;

import cbui.Component;

public class Lorem extends Component {

    private static final String lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";

    @Override
    public void onUpdate(long delta) {
    }

    @Override
    public void onRedraw() {
        int i = 0;
        for (int ny = 0; ny < getHeight(); ++ny) {
            for (int nx = 0; nx < getWidth(); ++nx) {
                if (i < lorem.length()) {
                    print(nx, ny, lorem.charAt(i));
                    ++i;
                }
            }
        }
    }
}
