package cbui.component;

import cbui.Component;
import cbui.event.MouseButtonEvent;
import cbui.event.listener.MouseButtonListener;

public class Button extends Component implements MouseButtonListener {

    private String text;

    private Runnable onClick;

    public Button(String text) {
        super(text.length() + 1, 1);
        this.text = text;
        addMouseButtonListener(this);
    }

    public Button(String text, Runnable onClick) {
        this(text);
        this.onClick = onClick;
    }

    @Override
    public void onUpdate(long delta) {
    }

    @Override
    public void onRedraw() {
        // TODO fill with background color
        print(0, 0, text);
    }

    @Override
    public void onMouseButtonPressed(MouseButtonEvent event) {
        if (onClick != null) {
            onClick.run();
        }
    }

    @Override
    public void onMouseButtonReleased(MouseButtonEvent event) {
    }

    public void setOnClick(Runnable onClick) {
        this.onClick = onClick;
    }
}
