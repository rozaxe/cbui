package cbui.component;

import cbui.Component;
import cbui.Unicode;
import cbui.event.KeyCode;
import cbui.event.listener.KeyListener;
import cbui.event.listener.TextListener;

import java.awt.*;
import java.util.function.Consumer;

public class Input extends Component implements TextListener, KeyListener {

    private final StringBuilder input = new StringBuilder();
    private int size;
    private int index = 0;
    private int length = 0;

    private Consumer<String> onEnter;

    /**
     * Create an input leaf that can hold typed character
     * It requires one width unit more for processing the cursor
     *
     * @param size the input's max capacity
     */
    public Input(int size) {
        super(size + 1, 1); // + 1 for the cursor
        this.size = size;
        input.ensureCapacity(size);

        addTextListener(this);
        addKeyListener(this);
    }

    /**
     * Get input's value
     *
     * @return the input's value
     */
    public final String getValue() {
        return input.toString();
    }

    /**
     * Set input's value
     *
     * @param value the value that input should be, if the value length is greeter than the input capacity,
     *              the value is trim to fit the capacity
     */
    public final void setValue(String value) {
        input.setLength(0); // Empty input
        input.append(value); // Fill it
        if (value.length() > size) { // Trim it if needed
            input.setLength(size);
        }
        length = input.length();
        index = length;
    }

    /**
     * Called when BACKSPACE is typed
     */
    protected void processBackspace() {
        if (index > 0) {
            length = Math.max(length - 1, 0);
            index = Math.max(index - 1, 0);
            input.deleteCharAt(index);
        }
    }

    /**
     * Called when a character is typed
     *
     * @param character the character typed
     */
    protected void processCharacter(char character) {
        if (length < size) {
            // Add character only if input is less than max size

            if (index == length) {
                input.append(character);

            } else {
                input.insert(index, character);
            }
            ++length;
            ++index;
        }
    }

    /**
     * Called when ENTER is typed
     */
    protected void processEnter() {
        if (onEnter != null) {
            onEnter.accept(getValue());
        }
    }

    @Override
    public final void onUpdate(long delta) {
    }

    @Override
    public final void onRedraw() {
        // Draw input
        for (int i = 0; i < length; ++i) {
            print(i, 0, input.charAt(i));
        }

        // Draw cursor under text
        if (index < length) {
            print(index, 0, input.charAt(index), Color.BLACK, Color.WHITE);
        } else {
            print(index, 0, Unicode.FULL_BLOCK);
        }
    }

    @Override
    public final void onTextEntered(char character) {
        if (character == KeyCode.BACKSPACE) {
            processBackspace();

        } else if (character == KeyCode.ENTER) {
            processEnter();

        } else {
            processCharacter(character);
        }
    }

    @Override
    public final void onKeyPressed(int code) {
        switch (code) {
            case KeyCode.LEFT:
                index = Math.max(index - 1, 0);
                break;

            case KeyCode.RIGHT:
                index = Math.min(index + 1, length);
                break;
        }
    }

    @Override
    public final void onKeyReleased(int code) {
    }

    public void setOnEnter(Consumer<String> onEnter) {
        this.onEnter = onEnter;
    }
}
