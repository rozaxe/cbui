package cbui.component;

import cbui.composite.*;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Vector;
import java.util.function.Consumer;

public final class SelectFolder extends SceneContainer {

    private final ListFolder listFolder;
    private final Consumer<String> onChoose;
    private final Runnable onCancel;

    public SelectFolder(@NotNull Consumer<String> onChoose, @Nullable Runnable onCancel) {

        this.onChoose = onChoose;
        this.onCancel = onCancel;

        // After because it needs access to given function
        listFolder = new ListFolder();


        // TODO Add "new folder" scene
        add("base", new Scene(listFolder));
        start("base");
    }

    private void processOnChoose() {
        String fullPath = listFolder.directory.getValue();
        fullPath += File.separator;
        fullPath += listFolder.selection.getSelectedAsString();

        onChoose.accept(fullPath);
    }

    private class ListFolder extends Stack {

        final Input directory;
        final Select selection;
        int lastSelected;

        ListFolder() {
            super(Orientation.Vertical);

            // Absolute folder path
            directory = new Input(128);

            // Inner selection folder
            selection = new Select(new String[]{""}, 0);
            selection.setOnSelect(this::onSelect);

            // Button to submit choice
            Button choose = new Button("Choose", SelectFolder.this::processOnChoose);
            Button cancel = new Button("Cancel", onCancel);

            // Add all to composite
            add(directory);
            addSeparator();
            add(selection);
            addSeparator();
            add((new Inline()).pipe(choose).pipe(cancel));

            // Default to home directory
            setToHome();
        }

        private void setToHome() {
            String homeDirectory = System.getProperty("user.home");
            directory.setValue(homeDirectory);
            selection.setOptions(getFolders(homeDirectory));
            lastSelected = 0;
        }

        private void onSelect(int select) {
            if (lastSelected == select) {
                // Double click on folder
                String base = directory.getValue();
                String append = selection.getSelectedAsString();
                String fullPath = base + File.separator + append;

                Path sanitize = Paths.get(fullPath).normalize();

                if (Files.exists(sanitize)) {
                    // Enter folder
                    directory.setValue(sanitize.toString());
                    selection.setOptions(getFolders(sanitize.toString()));
                    lastSelected = 0;

                } else {
                    // Reset to home directory
                    setToHome();
                }

            } else {
                lastSelected = select;
            }
        }

        // Get all folder's name inside a given directory
        private String[] getFolders(String directory) {
            File folder = new File(directory);
            File[] filesList = folder.listFiles();

            Vector<String> foldersName = new Vector<>();

            // Add current directory
            foldersName.add(".");

            // Add parent if exists
            if (folder.getParent() != null) {
                foldersName.add("..");
            }

            // Add visible folder
            for (File f : filesList) {
                if (f.isDirectory() && !f.isHidden()) {
                    foldersName.add(f.getName());
                }
            }

            return foldersName.toArray(new String[foldersName.size()]);
        }
    }
}
