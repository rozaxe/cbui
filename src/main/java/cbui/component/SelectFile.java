package cbui.component;

import cbui.composite.*;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Vector;
import java.util.function.Consumer;

public final class SelectFile extends SceneContainer {

    private final ListFolder listFolder;
    private final Consumer<String> onChoose;
    private final Runnable onCancel;

    public SelectFile(@NotNull Consumer<String> onChoose, @Nullable Runnable onCancel) {

        this.onChoose = onChoose;
        this.onCancel = onCancel;
        this.listFolder = new ListFolder();

        add("base", new Scene(listFolder));
        start("base");
    }

    private void processOnChoose() {
        String fullPath = listFolder.directory.getValue();
        fullPath += File.separator;
        fullPath += listFolder.selection.getSelectedAsString();

        // TODO Treat open folder

        onChoose.accept(fullPath);
    }

    private class ListFolder extends Stack {

        final Input directory;
        final Select selection;
        int lastSelected;

        ListFolder() {
            super(Orientation.Vertical);

            // Absolute folder path
            directory = new Input(128);

            // Inner selection folder
            selection = new Select(new String[]{""}, 0);
            selection.setOnSelect(this::onSelect);

            // Button to submit choice
            Button choose = new Button("Choose", SelectFile.this::processOnChoose);
            Button cancel = new Button("Cancel", onCancel);

            // Add all to composite
            add(directory);
            addSeparator();
            add(selection);
            addSeparator();
            add((new Inline()).pipe(choose).pipe(cancel));

            setToHome();
        }

        private void onSelect(int select) {
            if (lastSelected == select) {
                // Double click on folder
                String base = directory.getValue();
                String append = selection.getSelectedAsString();
                String fullPath = base + File.separator + append;

                Path sanitize = Paths.get(fullPath).normalize();

                if (Files.exists(sanitize)) {
                    if (Files.isDirectory(sanitize)) {
                        // Enter folder
                        directory.setValue(sanitize.toString());
                        selection.setOptions(getFolders(sanitize.toString()));
                        lastSelected = 0;

                    } else {
                        // Double click on file
                        processOnChoose();

                    }

                } else {
                    // Reset to home directory
                    setToHome();
                }

            } else {
                lastSelected = select;
            }
        }

        private void setToHome() {
            String homeDirectory = System.getProperty("user.home");
            directory.setValue(homeDirectory);
            selection.setOptions(getFolders(homeDirectory));
            lastSelected = 0;
        }


        // Get all folder's name inside a given directory
        private String[] getFolders(String directory) {
            File folder = new File(directory);
            File[] filesList = folder.listFiles();

            Vector<String> filesName = new Vector<>();

            // Add parent
            if (folder.getParent() != null) {
                filesName.add("..");
            }

            // Add visible folder
            for (File f : filesList) {
                if (!f.isHidden()) {
                    filesName.add(f.getName());
                }
            }

            return filesName.toArray(new String[filesName.size()]);
        }
    }
}
