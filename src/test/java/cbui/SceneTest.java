package cbui;

import cbui.composite.Scene;
import cbui.composite.SceneContainer;
import cbui.event.KeyCode;
import cbui.event.listener.KeyListener;

public class SceneTest extends Board {

    private SceneTest() {
        super("States", 64, 16, false, "Unifont.ttf", 16f, 30);

        SceneContainer manager = new SceneContainer();

        manager.add("A", new MenuA(manager));
        manager.add("B", new MenuB(manager));

        manager.start("A"); // Let's start with A, not B (last added)

        set(manager);

    }

    public static void main(String[] args) {
        (new SceneTest()).start();
    }

    private class MenuA extends Scene {

        public MenuA(SceneContainer manager) {
            set(new MenuBase(manager, 'A', KeyCode.A, "B"));
        }

        @Override
        public void onCreate() {
            System.out.println("A is created");
        }

        @Override
        public void onDispose() {
            System.out.println("A is disposed");
        }

    }

    private class MenuB extends Scene {

        public MenuB(SceneContainer manager) {
            set(new MenuBase(manager, 'B', KeyCode.B, "A"));
        }

        @Override
        public void onCreate() {
            System.out.println("B is created");
        }

        @Override
        public void onDispose() {
            System.out.println("B is disposed");
        }

    }

    private class MenuBase extends Component implements KeyListener {

        private SceneContainer manager;
        private char letter;
        private int key;
        private String next;

        public MenuBase(SceneContainer manager, char letter, int key, String next) {
            this.manager = manager;
            this.letter = letter;
            this.key = key;
            this.next = next;
            addKeyListener(this);
        }

        @Override
        public void onUpdate(long delta) {
        }

        @Override
        public void onRedraw() {
            print(1, 1, "Press " + letter + " to continue...");
        }

        @Override
        public void onKeyPressed(int code) {
            if (code == key) {
                manager.start(this.next);
            }
        }

        @Override
        public void onKeyReleased(int code) {
        }
    }

}
