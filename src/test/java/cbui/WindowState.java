package cbui;

import cbui.event.listener.WindowListener;

public class WindowState extends Component implements WindowListener {

    private boolean focused = true;

    private WindowState() {
        addWindowListener(this);
    }

    public static void main(String[] args) {
        Board board = new Board("WindowState", 20, 6, false, "Unifont.ttf", 16f, 30);
        board.set(new WindowState());
        board.start();
    }

    @Override
    public void onUpdate(long delta) {
    }

    @Override
    public void onRedraw() {
        print(0, 0, (focused ? "F" : "Not f") + "ocused");
    }

    @Override
    public void onBlur() {
        focused = false;
    }

    @Override
    public void onClose() {
        System.out.println("Closed");
    }

    @Override
    public void onFocus() {
        focused = true;
    }

}
