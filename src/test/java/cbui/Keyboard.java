package cbui;

import cbui.event.KeyCode;
import cbui.event.listener.KeyListener;
import cbui.event.listener.TextListener;

public class Keyboard extends Component implements KeyListener, TextListener {

    private String direction = "";
    private String text = "";

    private Keyboard() {
        addKeyListener(this);
        addTextListener(this);
    }

    public static void main(String[] args) {
        Board board = new Board("Keyboard", 50, 2, false, "Unifont.ttf", 16f, 30);
        board.set(new Keyboard());
        board.start();
    }

    @Override
    public void onUpdate(long delta) {
    }

    @Override
    public void onRedraw() {
        print(0, 0, direction);
        print(0, 1, text);
    }

    @Override
    public void onKeyPressed(int code) {
        switch (code) {
            case KeyCode.LEFT:
                direction = "LEFT";
                break;

            case KeyCode.DOWN:
                direction = "DOWN";
                break;

            case KeyCode.RIGHT:
                direction = "RIGHT";
                break;

            case KeyCode.UP:
                direction = "UP";
                break;
        }
    }

    @Override
    public void onKeyReleased(int code) {
    }

    @Override
    public void onTextEntered(char character) {
        if (character == KeyCode.BACKSPACE || character == KeyCode.DELETE) {
            if (text.length() > 0) {
                text = text.substring(0, text.length() - 1);
            }
        } else {
            if (text.length() < getWidth()) {
                text = text + character;
            }
        }
    }
}
