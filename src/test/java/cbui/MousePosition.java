package cbui;

import cbui.event.MouseEvent;
import cbui.event.listener.MouseListener;

public class MousePosition extends Component implements MouseListener {

    private int x = 0;
    private int y = 0;
    private boolean mouseIn = false;

    private MousePosition() {
        addMouseListener(this);
    }

    public static void main(String[] args) {
        Board board = new Board("MousePosition", 50, 20, false, "Unifont.ttf", 16f, 30);
        board.set(new MousePosition());
        board.start();
    }

    @Override
    public void onUpdate(long delta) {
    }

    @Override
    public void onRedraw() {
        print(0, 0, "x = " + x);
        print(0, 1, "y = " + y);
        print(0, 2, mouseIn ? "mouseIn" : "mouseOut");
    }

    @Override
    public void onMouseIn() {
        mouseIn = true;
    }

    @Override
    public void onMouseOut() {
        mouseIn = false;
    }

    @Override
    public void onMouseMoved(MouseEvent event) {
        x = event.x;
        y = event.y;
    }

}
