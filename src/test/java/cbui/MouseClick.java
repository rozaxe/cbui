package cbui;

import cbui.event.MouseButtonEvent;
import cbui.event.listener.MouseButtonListener;

import java.awt.*;
import java.util.Vector;

public class MouseClick extends Component implements MouseButtonListener {

    private Vector<Point> points = new Vector<>();

    private MouseClick() {
        addMouseButtonListener(this);
    }

    public static void main(String[] args) {
        Board board = new Board("MouseClick", 50, 20, false, "Unifont.ttf", 16f, 30);
        board.set(new MouseClick());
        board.start();
    }

    @Override
    public void onUpdate(long delta) {
    }

    @Override
    public void onRedraw() {
        for (Point point : points) {
            print(point.x, point.y, "X");
        }
    }

    @Override
    public void onMouseButtonPressed(MouseButtonEvent event) {
        points.add(new Point(event.x, event.y));
    }

    @Override
    public void onMouseButtonReleased(MouseButtonEvent event) {

    }
}
