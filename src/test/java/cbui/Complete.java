package cbui;

import cbui.component.FrameCount;
import cbui.component.Input;
import cbui.component.Lorem;
import cbui.component.Select;
import cbui.composite.Orientation;
import cbui.composite.Stack;
import cbui.event.MouseButtonEvent;
import cbui.event.MouseEvent;
import cbui.event.listener.MouseButtonListener;
import cbui.event.listener.MouseListener;

public class Complete extends Board {

    private Complete() {
        super("Complete", 60, 20, false, "Unifont.ttf", 16f, 30);

        // Hide column
        Stack hideColumn = new Stack(Orientation.Vertical);
        hideColumn.add(new Select(new String[]{"lorem", "ipsum", "dolor"}, 3));
        hideColumn.addSeparator();
        hideColumn.add(new Hiding());

        // Lorem column
        Stack loremColumn = new Stack(Orientation.Vertical);
        loremColumn.add(new Lorem());
        loremColumn.addSeparator();
        loremColumn.add(new Lorem());
        loremColumn.addSeparator();
        loremColumn.add(new Lorem());

        // Middle line
        Stack middleLine = new Stack(Orientation.Horizontal);
        middleLine.add(new Lorem());
        middleLine.addSeparator();
        middleLine.add(hideColumn);
        middleLine.addSeparator();
        middleLine.add(loremColumn);

        // Main Column
        Stack mainColumn = new Stack(Orientation.Vertical);
        mainColumn.add(new FrameCount());
        mainColumn.addSeparator();
        mainColumn.add(middleLine);
        mainColumn.addSeparator();
        //mainColumn.add(new Echo(0, 1));
        mainColumn.add(new Input(60));

        // Add it to board
        set(mainColumn);
    }

    public static void main(String[] args) {
        (new Complete()).start();
    }

    private class Hiding extends Component implements MouseListener, MouseButtonListener {

        boolean hide = true;

        int cx = 0;
        int cy = 0;

        char press = '@';

        Hiding() {
            addMouseListener(this);
            addMouseButtonListener(this);
        }

        @Override
        public void onUpdate(long delta) {
        }

        @Override
        public void onRedraw() {
            if (hide) {

                for (int x = 0; x < getWidth(); ++x) {
                    for (int y = 0; y < getHeight(); ++y) {
                        print(x, y, '*');
                    }
                }

            } else {

                print(cx, cy, press);

            }
        }

        @Override
        public void onMouseIn() {
            hide = false;
        }

        @Override
        public void onMouseOut() {
            hide = true;
        }

        @Override
        public void onMouseMoved(MouseEvent event) {
            cx = event.x;
            cy = event.y;
        }

        @Override
        public void onMouseButtonPressed(MouseButtonEvent event) {
            press = '%';
        }

        @Override
        public void onMouseButtonReleased(MouseButtonEvent event) {
            press = '@';
        }
    }

}
