package cbui;

import java.awt.*;

public class RGBA extends Printer {

    private int lastFPS = 0;
    private int currentFPS = 0;
    private long delta = 0;

    private RGBA() {
        super("RGBA", 10, 5, false, "Unifont.ttf", 16f, 30);
    }

    public static void main(String[] args) {
        (new RGBA()).start();
    }

    @Override
    public void onUpdate(long delta) {

        ++currentFPS;

        this.delta += delta;
        if (this.delta >= 1e9) {
            lastFPS = currentFPS;
            this.delta = 0;
            currentFPS = 0;
        }

        print(0, 1, "R", Color.RED, Color.BLACK);
        print(1, 1, "G", Color.GREEN, Color.BLACK);
        print(2, 1, "B", Color.BLUE, Color.BLACK);
        print(3, 1, "A", Color.WHITE, Color.BLACK);

        print(0, 0, String.valueOf(lastFPS), Color.WHITE, Color.BLACK);
    }
}
